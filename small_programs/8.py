#/usr/bin/python3
'''
If the number is divisible by 2, then we called it as even number. And the remaining ones (not divisible by 2) called as odd numbers.

'''
# To get a number from user
number = int(input("Please Enter any value: "))

# To check If number is Even or Odd
if number%2 == 0:
    print (number, "is an even number")
elif number%2 == 1:
    print ( number, "is and odd number")
else:
    print ("Error, Invalid input")
