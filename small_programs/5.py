'''
This python program allows a user to enter any character. 
Next, we use If Else Statement to check whether the user given character is an Vowel or Consonant.
Here, we are using If Statement to check the String character is a, e, i, o, u, A, E, I, O, U
If it is TRUE, it is an Vowel; otherwise, it is an Consonant
'''
# To get input from user
ch = input("Please Enter a Character: ")

# To check whether it is a vowel or consonant


if ( ch=='A' or ch=='a' or ch=='E' or ch=='e' or ch=='I' or ch=='i' or ch=='O' or ch=='o' or ch=='U' or ch=='u'):
    print ("The Given character", ch, "is a Vowel")
else:
    print ("The Given character", ch, "is a Consonant")
