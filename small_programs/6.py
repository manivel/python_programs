#/usr/bin/python3
'''
1.To add two numbers allows the user to enter two values. 
2.Next, it will add those two numbers and assign the total to variable sum
The reason we are using the float() function over the input() function is because the input() function receives the value as String, so to convert it into a number we are using the float().
'''
# To get value from user
value_1 = float(input("Please Enter the first value: "))
value_2 = float(input("Please Enter the second value: "))

# To sum two numbers
sum = value_1 + value_2
print ("The sum of given value is", sum)
