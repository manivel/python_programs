#/usr/bin/python3
'''
1.Any natural number that is not divisible by any other number except 1 and itself called Prime Number in Python
2.Number should be grater than 1
To loop through a set of code a specified number of times, we can use the range() function,
The range() function defaults to 0 as a starting value, however it is possible to specify the starting value by adding a parameter: range(2, 6), which means values from 2 to 6 (but not including 6):
'''
# To get any numbers from user
number = int(input("Please Enter any number: "))

# To check whether the given number is a Prime number or Not using loop
if number > 1:
    for i in range(2, number):
        if (number % i) == 0:
            print ("The given number", number, "is not a prime number")
            break
        else:
            print ("The given number", number, "is a prime number")
else:
            
    print ("The given number", number, "is not a prime number")
