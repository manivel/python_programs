'''
This python program allows a user to enter any character. 
Next, we use If Else Statement to check whether the user given character is an alphabet or not. 
Here, If statement checks the character is between a and z or between A and Z. 
If it is TRUE, it is an alphabet; otherwise, it is not an alphabet
'''
# Get input from user
ch = input("Please Enther a Character: ")

# To check whether it is a lowercase or uppercase
if (( ch>='a' and ch<='z') or ( ch>='A' and ch<='Z' )):
    print ( "The Given Character", ch, "is an Alphapet")
else:
    print ( "The Given Character", ch, "is not an Alphabet")
