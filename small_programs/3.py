'''
1. Logically, All the years that are perfectly divisible by four called Leap years except the century years.
2. Century years mean they end with 00 such as 1200, 1300, 2400, 2500, etc. (Obviously they are divisible by 100). For these century years, we have to calculate further to check the Leap year in Python.
    a. If the century year is divisible by 400, then that year is a Leap year in Python.
    b. If it is not divisible by 400, then that year is not a Python Leap year.
'''
# To get the year
year = int(input("Enter the year: "))

# To check whether it is a leap year or not
if year % 4 == 0 or year % 400 == 0 and year % 100 != 0:
    print (year, "is a leap year")
elif year % 100 == 0:
    print (year, "is not leap year")
else :
    print (year, "is not a leap year")

